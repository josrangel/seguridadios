// Allow the popup password window
$(document).ready(function() {
	$('body').on('click', '.loginLink', function() {
		event.preventDefault();
		document.getElementById('hiddenAlbumID').value = $(this).attr('data-albumUDID');
        $(".overlay").fadeToggle("fast");
    });

	
	$("#uploadButton").click(function( event ){
		event.preventDefault();
    	$(".overlay").fadeToggle("fast");
  	});

	$("#close").click(function(){
		$(".overlay").fadeToggle("fast");
	});
	
	$("#closeAndReloadLastPage").click(function(){
		// see if any items have been added, if so load last page
		if (document.querySelectorAll("div #drop li").length > 0) {
			window.location.replace("album?page=0&albumUDID=".concat($(this).attr('data-albumUDID')));
		}
		$(".overlay").fadeToggle("fast");
	});
	
	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
			event.preventDefault();
			$(".overlay").fadeToggle("fast");
		}
	});
	
	$('.image-link').magnificPopup({ 
	  type: 'image'
		// other options
	});
});

// Image grid
;( function( $, window, document, undefined )
{
	'use strict';

	var s = document.body || document.documentElement, s = s.style;
	if( s.webkitFlexWrap == '' || s.msFlexWrap == '' || s.flexWrap == '' ) return true;

	var $list		= $( '.list' ),
		$items		= $list.find( '.list__item__inner' ),
		setHeights	= function()
	    {
			$items.css( 'height', 'auto' );

			var perRow = Math.floor( $list.width() / $items.width() );
			if( perRow == null || perRow < 2 ) return true;

			for( var i = 0, j = $items.length; i < j; i += perRow )
			{
				var maxHeight	= 0,
					$row		= $items.slice( i, i + perRow );

				$row.each( function()
				{
					var itemHeight = parseInt( $( this ).outerHeight() );
					if ( itemHeight > maxHeight ) maxHeight = itemHeight;
				});
				$row.css( 'height', maxHeight );
			}
		};

	setHeights();
	$( window ).on( 'resize', setHeights );
	$list.find( 'img' ).on( 'load', setHeights );

})( jQuery, window, document );

