#include <stdio.h> 
#include <stdlib.h> // For exit() 
  
int main() 
{ 
    FILE *file; 
  
    char c; 
    
    // Open file 
    file = fopen("/var/root/cristian/texto.txt", "r"); 
    if (file == NULL) 
    { 
        printf("Cannot open file \n"); 
        exit(0); 
    } 
  
    // Read contents from file 
    c = fgetc(file); 
    while (c != EOF) 
    { 
        fprintf (stdout, "%c", c); 
        c = fgetc(file); 
    } 
  
    fclose(file); 
    return 0; 
}
