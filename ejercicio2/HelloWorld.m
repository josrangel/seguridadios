#import <Foundation/Foundation.h>


@interface SaySomething : NSObject
- (void) say: (NSString *) phrase;
@end

@implementation SaySomething

- (void) say: (NSString *) phrase {
        printf("%s\n", [ phrase UTF8String ]);
}
@end

int main(void) {
	@autoreleasepool {
		SaySomething *saySomething = [ [ SaySomething alloc ] init ];
		[ saySomething say: @"Hello, world!" ];
		[ saySomething release ];
		return 0;
	}
}
